var middleware = require('../middleware/middleware');


module.exports = function(app) {
    var controlador = require('../controller/controller');

    app.post('/login',controlador.login);
    app.get('/usuarios/get',middleware.checkToken, controlador.getUsuarios);
    app.put('/usuarios/put',middleware.checkToken, controlador.putUsuarios);
}