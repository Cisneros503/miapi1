var express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

const cors = require('cors');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());


var routes = require('./routes/routes');
routes(app);

var server = app.listen(port, function() {
    console.log(`Server Corriendo en ${port}`);
});